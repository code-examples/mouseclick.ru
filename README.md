# [mouseclick.ru](https://mouseclick.ru) source codes

<br/>

### Run mouseclick.ru on localhost

    # vi /etc/systemd/system/mouseclick.ru.service

Insert code from mouseclick.ru.service

    # systemctl enable mouseclick.ru.service
    # systemctl start mouseclick.ru.service
    # systemctl status mouseclick.ru.service

http://localhost:4042
